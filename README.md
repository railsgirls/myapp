# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: 2.4.2
* Rails version: 5.1.4
* System dependencies: Ruby and Rails installed, Homebrew, Ruby versions'
manager (rbenv or rvm), rake (for running rake tasks)
* Configuration
* Database creation: SQLite3
* Database initialization
* How to run the test suite
* Services (job queues, cache servers, search engines, etc.)
* Deployment instructions

----

References:

- Tutorials: http://guides.railsgirls.com/app
- RailsGirls @ GitLab: https://gitlab.com/gitlab-com/peopleops/issues/463
